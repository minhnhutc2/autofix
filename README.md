[Install project] <br>
1: cd docker <br>
2: cp .env.example .env <br>
3: edit config docker <br>
4: docker compose build <br>
5: docker compose up -d <br>
6: docker exec -it autofix_php bash <br>
7: composer install <br>
