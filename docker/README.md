# Setup
1: Create folder project (Ex: qtest). <br>
2: cd qtest <br>
3: Clone docker: git clone https://gitlab.com/minhnhutc2/nta-co2-docker.git<br>
4: Clone source: git clone https://gitlab.com/minhnhutc2/nta-co2-qtest.git<br>
5: cd qtest and checkout to branch develop: git checkout develop <br>
6: cd qtest_docker <br>
7: cp .env.example .env (Có thể thay đổi giá trị mong muốn trong .env)<br>
8: Run command: <br>
- docker-compose build --no-cache <br>
- docker-compose up -d
- docker exec -it qtest_local bash
- composer install
- cp .env.example .env
- php artisan key:generate
- php artisan migrate --seed
- php artisan jwt:secret

9: Link access: (Thay đổi port giống như setting trong .env)
- site: http://localhost:8088
- phpMyadmin: http://localhost:8089
- mailcatcher: http://localhost:1088
